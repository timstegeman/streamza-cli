package main

import (
	"net/http"
	"fmt"
	"log"
	"strings"
	"github.com/urfave/cli"
	"time"
)

func submitMagnet(writer http.ResponseWriter, request *http.Request) {
	magnetUri := request.FormValue("q")

	if !strings.HasPrefix(magnetUri, "dllink ") {
		log.Printf("Invalid data: %s\n", magnetUri)
		http.Error(writer, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		fmt.Fprint(writer, "Failed")
		return
	}

	magnetUri = magnetUri[7:]
	log.Printf("Request for downloading magnet %s\n", magnetUri)

	_, err := client.Add(magnetUri)
	if err != nil {
		log.Printf("Failed to add magnet %q\n", err)
		http.Error(writer, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		fmt.Fprint(writer, "Failed")
		return
	}

	log.Println("Magnet added successfuly")
	fmt.Fprint(writer, "OK")
}

func startDaemonAction(c *cli.Context) {
	destDir := c.String("dest")
	listenOn := c.String("listen")
	interval := c.Int("interval")

	// start ticker to check for downloads
	ticker := time.NewTicker(time.Second * time.Duration(interval))
	go func() {
		for range ticker.C {
			checkDownloads(destDir)
		}
	}()

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprint(writer, "OK")
	})
	http.HandleFunc("/submit", submitMagnet)
	log.Printf("Daemon started on %s", listenOn)
	log.Fatal(http.ListenAndServe(listenOn, nil))
}

func checkDownloads(destDir string) {
	log.Println("Checking for completed downloads")

	torrents, err := client.List()
	if err != nil {
		log.Println(err)
		return
	}

	for _, torrent := range *torrents {
		if torrent.State == "Completed" {
			log.Printf("Download torrent %s\n", torrent.Name)
			err = downloadTorrentFiles(torrent.Id, destDir)
			if err != nil {
				log.Println(err)
				continue
			}
			client.Delete(torrent.Id)
		}
	}
}
