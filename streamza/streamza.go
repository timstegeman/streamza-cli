package streamza

import (
	"fmt"
	"log"
	"net/http"
	"net/http/cookiejar"
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
	"net/url"
	"strings"
	"strconv"
	"io/ioutil"
	"regexp"
)

type Client interface {
	Login(username string, password string) (error)
	Logout() (error)

	List() (*[]Torrent, error)
	Get(id int) (*Torrent, error)
	Add(magnetUri string) (int, error)
	Delete(id int) (error)
}

type ClientOp struct {
	client *http.Client
}

type Torrent struct {
	Id    int
	Name  string
	State string
	Files [] TorrentFile
}

type TorrentFile struct {
	Id      int
	Name    string
	Details string
	Url     string
}

type CustomTransport struct{}

func (t *CustomTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("User-Agent", "StreamzaCli/0.1 (https://bitbucket.org/timstegeman/streamza-cli/)")
	return http.DefaultTransport.RoundTrip(req)
}

func NewClient() *ClientOp {
	jar, err := cookiejar.New(&cookiejar.Options{})
	if err != nil {
		log.Fatal(err)
	}
	client := &http.Client{
		Jar:       jar,
		Transport: &CustomTransport{},
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	return &ClientOp{client: client}
}

func (c *ClientOp) Login(username string, password string) (error) {
	resp, err := c.client.PostForm("https://streamza.com/gettingstarted.php?action=signin",
		url.Values{"name": {username}, "password": {password}, "login_btn": {"Log In"}})
	if err != nil {
		return err
	}

	if resp.StatusCode != 302 {
		return fmt.Errorf("invalid username and/or password")
	}

	return nil
}

func (c *ClientOp) Logout() (error) {
	if _, err := c.client.Get("https://streamza.com/logout.php"); err != nil {
		return err
	}

	return nil
}

func (c *ClientOp) List() (*[]Torrent, error) {
	resp, err := c.client.Get("https://streamza.com/user_transmission.php")
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data map[string]interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(data["html"].(string)))
	if err != nil {
		return nil, err
	}

	var torrents []Torrent
	doc.Find(".torrent-item").Each(func(i int, s *goquery.Selection) {
		var torrent Torrent
		re := regexp.MustCompile("[^0-9]")
		id := re.ReplaceAllLiteralString(s.AttrOr("id", ""), "")
		name := s.Find("span.dwn_title").Text()
		var state = ""
		s.Find(".other_details span").Each(func(i int, s *goquery.Selection) {
			state += s.Text() + ", "
		})

		if state != "" {
			state += "Progress: " + re.ReplaceAllLiteralString(s.Find(".progress").AttrOr("style", "0"), "") + "%"
		} else {
			state = strings.TrimSpace(s.Find(".other_details").Text())
		}

		if state == "" {
			state = "Completed"
		}

		torrent.Id, _ = strconv.Atoi(id)
		torrent.Name = name
		torrent.State = state

		torrents = append(torrents, torrent)
	})

	return &torrents, nil
}

func (c *ClientOp) Get(id int) (*Torrent, error) {
	resp, err := c.client.PostForm("https://streamza.com/torrents.php",
		url.Values{"torrent_id": {strconv.Itoa(id)}, "resume_data": {""}})
	if err != nil {
		return nil, err
	}

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}

	title := doc.Find("h2").AttrOr("title", "")

	var files []TorrentFile
	doc.Find(".btn_cont").Each(func(i int, s *goquery.Selection) {
		id := s.Parent().Find(".content_number p").Text()
		name := strings.TrimSpace(s.Parent().Find(".content_list p").Text())
		details := s.Parent().Find(".content_list .content_details span").First().Text()
		downloadUri := s.Find("a").First().AttrOr("onclick", "")
		downloadUri = downloadUri[strings.Index(downloadUri, "'")+1: strings.LastIndex(downloadUri, "'")]

		var file TorrentFile
		file.Id, _ = strconv.Atoi(id)
		file.Name = name
		file.Details = details
		file.Url = downloadUri

		files = append(files, file)
	})

	var torrent Torrent
	torrent.Name = title
	torrent.Id = id
	torrent.Files = files

	return &torrent, nil
}

func (c *ClientOp) Add(magnetUri string) (int, error) {
	resp, err := c.client.PostForm("https://streamza.com/torrent_add.php",
		url.Values{
			"type":       {"torrent"},
			"value":      {magnetUri},
			"id":         {"0"},
			"size":       {"0"},
			"hash":       {"0"},
			"category":   {"0"},
			"quality":    {"1"},
			"safesearch": {"0"},
		})
	if err != nil {
		return -1, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return -1, err
	}

	var data map[string]interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return -1, err
	}

	if data["msge"] != nil {
		return -1, fmt.Errorf(data["msge"].(string))
	}

	return strconv.Atoi(data["torrent_id"].(string))
}

func (c *ClientOp) Delete(id int) (error) {
	if _, err := c.client.PostForm("https://streamza.com/torrent_delete.php",
		url.Values{"value": {strconv.Itoa(id)}}); err != nil {
		return err
	}
	return nil
}
