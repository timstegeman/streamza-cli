package main

import (
	"bitbucket.org/timstegeman/streamza-cli/streamza"
	"os"
	"fmt"
	"github.com/urfave/cli"
	"strconv"
	"github.com/cavaliercoder/grab"
	"path"
	"log"
)

var (
	client streamza.Client
)

func init() {
	client = streamza.NewClient()
}

func main() {
	app := cli.NewApp()
	app.Name = "streamza-cli"
	app.Version = "0.1"
	app.Usage = "Streamza command line client"

	login := func(c *cli.Context) error {
		return client.Login(c.GlobalString("username"), c.GlobalString("password"))
	}
	logout := func(c *cli.Context) error {
		return client.Logout()
	}

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "username, u",
			Usage:  "Streamza account username",
			EnvVar: "STREAMZA_USERNAME",
		},
		cli.StringFlag{
			Name:   "password, p",
			Usage:  "Streamza account password",
			EnvVar: "STREAMZA_PASSWORD",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:      "list",
			Aliases:   []string{"l"},
			Usage:     "list all torrents",
			ArgsUsage: "",
			Action:    listTorrentsAction,
			Flags: []cli.Flag{
				cli.BoolFlag{Name: "completed", Usage: "Show completed torrents"},
			},
			Before: login,
			After:  logout,
		},
		{
			Name:      "get",
			Aliases:   []string{"g"},
			ArgsUsage: "[id]",
			Usage:     "get torrent details",
			Action:    getTorrentAction,
			Before:    login,
			After:     logout,
		},
		{
			Name:      "add",
			Aliases:   []string{"a"},
			ArgsUsage: "[magnetUri]",
			Usage:     "add torrent",
			Action:    addTorrentAction,
			Before:    login,
			After:     logout,
		},
		{
			Name:      "download",
			Aliases:   []string{"dl"},
			ArgsUsage: "[id]",
			Usage:     "download all torrent files",
			Action:    downloadTorrentAction,
			Flags: []cli.Flag{
				cli.StringFlag{Name: "dest", FilePath: ".", Usage: "Destination directory"},
				cli.BoolFlag{Name: "url", Usage: "Only print URL's to STDOUT"},
			},
			Before: login,
			After:  logout,
		},
		{
			Name:      "delete",
			Aliases:   []string{"d"},
			ArgsUsage: "[id]",
			Usage:     "delete torrent",
			Action:    deleteTorrentAction,
			Before:    login,
			After:     logout,
		},
		{
			Name:   "daemon",
			Usage:  "start the daemon and simulate a mlnet server",
			Action: startDaemonAction,
			Flags: []cli.Flag{
				cli.StringFlag{Name: "dest", FilePath: ".", Usage: "Destination directory"},
				cli.BoolFlag{Name: "delete", Usage: "Delete torrent on streamza after downloading"},
				cli.IntFlag{Name: "interval", Usage: "Check interval on completed torrents in seconds", Value: 300},
				cli.StringFlag{Name: "listen", Usage: "Host and port to listen on", Value: "127.0.0.1:4080"},
			},
			Before: login,
			After:  logout,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
	}
}

func listTorrentsAction(c *cli.Context) (error) {
	torrents, err := client.List()
	if err != nil {
		return err
	}

	onlyCompleted := c.Bool("completed")

	fmt.Printf("Id\tName\tState\n")
	for _, torrent := range *torrents {
		if onlyCompleted && torrent.State != "Completed" {
			continue
		}
		fmt.Printf("%d\t%s\t%s\n", torrent.Id, torrent.Name, torrent.State)
	}

	return nil
}

func getTorrentAction(c *cli.Context) (error) {
	id, err := strconv.Atoi(c.Args().Get(0))
	if err != nil {
		return err
	}

	torrent, err := client.Get(id)
	if err != nil {
		return err
	}
	fmt.Printf("ID: %d\n", torrent.Id)
	fmt.Printf("Name: %s\n\n", torrent.Name)
	fmt.Printf("Id\tName\tDetails\n")
	for _, file := range (*torrent).Files {
		fmt.Printf("%d\t%s\t%s\n", file.Id, file.Name, file.Details)
	}

	return nil
}

func addTorrentAction(c *cli.Context) (error) {
	magnetUri := c.Args().Get(0)

	id, err := client.Add(magnetUri)
	if err != nil {
		return err
	}
	fmt.Println(id)

	return nil
}

func deleteTorrentAction(c *cli.Context) (error) {
	id, err := strconv.Atoi(c.Args().Get(0))
	if err != nil {
		return err
	}

	err = client.Delete(id)
	if err != nil {
		return err
	}

	return nil
}

func downloadTorrentAction(c *cli.Context) (error) {
	id, err := strconv.Atoi(c.Args().Get(0))
	if err != nil {
		return err
	}

	if c.Bool("url") {
		torrent, err := client.Get(id)
		if err != nil {
			return err
		}

		for _, file := range (*torrent).Files {
			fmt.Println(file.Url)
		}
		return nil
	}

	return downloadTorrentFiles(id, c.String("dest"))
}

func downloadTorrentFiles(id int, destDir string) (error) {
	torrent, err := client.Get(id)
	if err != nil {
		return err
	}

	destDir = path.Clean(destDir)
	if len((*torrent).Files) > 1 {
		destDir = path.Join(destDir, torrent.Name)
	}
	os.MkdirAll(destDir, 0755)

	syncFilePath := path.Join(destDir, ".!sync")
	syncFile, err := os.Create(syncFilePath)
	if err != nil {
		log.Println("Unable to create sync file")
		return err
	}
	syncFile.Close()

	defer os.Remove(syncFilePath)

	for _, file := range (*torrent).Files {
		err = downloadUrl(file.Url, destDir)
		if err != nil {
			return err
		}
	}

	return nil
}

func downloadUrl(url string, destDir string) (error) {
	client := grab.NewClient()
	req, _ := grab.NewRequest(destDir, url)

	log.Printf("Downloading %v...\n", req.URL())
	resp := client.Do(req)
	log.Printf("  %v\n", resp.HTTPResponse.Status)

	if err := resp.Err(); err != nil {
		return resp.Err()
	}

	log.Printf("Download saved to ./%v \n", resp.Filename)
	return nil
}
